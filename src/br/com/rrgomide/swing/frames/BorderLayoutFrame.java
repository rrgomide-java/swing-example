package br.com.rrgomide.swing.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * Classe para demonstrar como o 
 * FlowLayout funciona
 * 
 * Criei com JDialog (e não JFrame)
 * para que eu consiga implementar o
 * comportamento "modal" (que não permite
 * a utilização de outras telas do programa
 * enquanto esta não for fechada)
 * 
 * @author Raphael
 *
 */
public class BorderLayoutFrame extends JDialog {

  /*
   * Botões a serem clicados pelo 
   * usuário
   */
  private JButton btnTop;
  private JButton btnLeft;
  private JButton btnCenter;
  private JButton btnRight; 
  private JButton btnBottom;
  
  /*
   * Vetor para conter os botões acima
   */
  private JButton buttons[];
  
  /*
   * Layout a ser controlado (BorderLayout)
   */
  BorderLayout mainLayout = new BorderLayout(5, 5);
  
  /**
   * Construtor da classe
   */
  public BorderLayoutFrame() {
    
    frameConfiguration();
    configButtons();
  }

  private void frameConfiguration() {
       
    this.setTitle("Testes com BorderLayout");
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.setSize(640, 480);
    this.setLocationRelativeTo(null);
    this.setModal(true);
    this.setLayout(mainLayout);
  }

  private void configButtons() {
    
    /*
     * Criando e dispondo botões
     * no layout
     */
    btnTop = new JButton("Topo");
    this.add(btnTop, BorderLayout.PAGE_START);
    
    btnBottom = new JButton("Fundo");
    this.add(btnBottom, BorderLayout.PAGE_END);
    
    btnLeft = new JButton("Esquerda");
    this.add(btnLeft, BorderLayout.LINE_START);
    
    btnRight = new JButton("Direita");
    this.add(btnRight, BorderLayout.LINE_END);
    
    btnCenter = new JButton("Centro");
    this.add(btnCenter, BorderLayout.CENTER); 
    
    /*
     * Criando e inicializando vetor
     */
    buttons    = new JButton[5];
    buttons[0] = btnTop;
    buttons[1] = btnBottom;
    buttons[2] = btnLeft;
    buttons[3] = btnRight;
    buttons[4] = btnCenter;
    
    /*
     * Criando e adicionando o mesmo evento 
     * para cada botão
     */
    ActionListener evento = new ButtonClick();
    
    for (JButton button : buttons)
      button.addActionListener(evento);
  }
  
  private class ButtonClick implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      
      /*
       * Percorrendo o vetor de botões. Para cada botão,
       * marcamos todos como visível, exceto o que foi
       * clicado, que fica invisível
       */
      for(JButton button : buttons) {
        
        if (button == e.getSource()) 
          button.setVisible(false);       
        else 
          button.setVisible(true);
      }      
      
      /*
       * Atualizando o layout
       */
      mainLayout.layoutContainer(BorderLayoutFrame.this.getContentPane());
    }    
  }
}
