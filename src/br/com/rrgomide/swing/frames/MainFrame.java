package br.com.rrgomide.swing.frames;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainFrame extends JFrame {
    
  private List<JButton> listButtons;
  private List<ActionListener> listActions;
  
  public MainFrame() {
       
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setSize(600, 200);
    this.setLocationRelativeTo(null);
    this.setTitle("Testes com Swing components");
    this.setResizable(false);
  
    createButtons();
    createActions(); 
    validateButtons();
    
    int numberOfRows = (int)Math.ceil(listButtons.size() / 2.00);    
    this.setLayout(new GridLayout(numberOfRows, 2, 5, 5));    
  }

  private void createButtons() {
    
    listButtons = new ArrayList<JButton>();
    listButtons.add(new JButton("Testes com FlowLayout"));
    listButtons.add(new JButton("Testes com BorderLayout"));
    listButtons.add(new JButton("Testes com GridLayout"));
    listButtons.add(new JButton("Testes com botões, labels e textedits"));
    listButtons.add(new JButton("Testes com checkBoxes e radioButtons"));
    listButtons.add(new JButton("Testes com comboBoxes e listboxes"));
    
    listButtons.forEach(button -> this.add(button));
  }

  private void createActions() {
    
    listActions = new ArrayList<ActionListener>();
    listActions.add(action -> new FlowLayoutFrame().setVisible(true));
    //listActions.add(action -> new BorderLayoutFrame().setVisible(true));
    
    for(int i = 0; i < listActions.size(); i++)
      listButtons.get(i).addActionListener(listActions.get(i));
  }

  private void validateButtons() {
    
    listButtons
      .forEach(
        button -> button.setEnabled(button.getActionListeners().length > 0)
      );
  }
}
