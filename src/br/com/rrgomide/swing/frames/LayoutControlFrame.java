package br.com.rrgomide.swing.frames;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class LayoutControlFrame extends JFrame {

  //PAGE_START
  private JRadioButton btnFlowLayout;
  private JRadioButton btnGridLayout;
  private ButtonGroup  layoutGroup;
  private JPanel pnlLayouts;
    
  //PAGE_END
  private JRadioButton btnNorth;
  private JRadioButton btnSouth;
  private JRadioButton btnEast;
  private JRadioButton btnWest;
  private ButtonGroup  borderLayoutGroup;
  private JPanel pnlBorderLayout;
  
  //CENTER
  
  
  private JFrame frameToManipulate;

  public LayoutControlFrame(JFrame _frameToManipulate) {
    
    setDefaultParameters();
    configLayoutButtons();
    configBorderLayoutButtons();
    //configFrameToManipulate(_frameToManipulate);
  }

  private void setDefaultParameters() {
    
    this.setLayout(new BorderLayout(5, 5));
    this.setSize(400, 200);
    this.setLocation(10, getMiddleHeight());
    this.setResizable(false);
  }

  private void configLayoutButtons() {

    pnlLayouts    = new JPanel(new FlowLayout());
    layoutGroup   = new ButtonGroup();
    
    btnFlowLayout = new JRadioButton("FlowLayout", true);
    btnFlowLayout.addActionListener(event -> setEnableBorderLayoutButtons(false));
    
    btnGridLayout = new JRadioButton("GridLayout", false);    
    btnGridLayout.addActionListener(event -> setEnableBorderLayoutButtons(true));
    
    layoutGroup.add(btnFlowLayout);
    layoutGroup.add(btnGridLayout);
    
    pnlLayouts.add(btnFlowLayout);
    pnlLayouts.add(btnGridLayout);
    this.add(pnlLayouts, BorderLayout.NORTH);
    //this.add(btnFlowLayout);
    //this.add(btnGridLayout);
  }

  private void configBorderLayoutButtons() {
    
    btnNorth = new JRadioButton("Page start", true);
    btnSouth = new JRadioButton("Page end", false);
    btnEast  = new JRadioButton("Line start", false);
    btnWest  = new JRadioButton("Line end", false);    
    borderLayoutGroup = new ButtonGroup();  
    pnlBorderLayout   = new JPanel(new FlowLayout());
    
    borderLayoutGroup.add(btnNorth);
    borderLayoutGroup.add(btnSouth);
    borderLayoutGroup.add(btnEast);
    borderLayoutGroup.add(btnWest);
    
    pnlBorderLayout.add(btnNorth);
    pnlBorderLayout.add(btnSouth);
    pnlBorderLayout.add(btnEast);
    pnlBorderLayout.add(btnWest);
    
    this.add(pnlBorderLayout, BorderLayout.SOUTH);
    
    setEnableBorderLayoutButtons(false);
  }

  private void setEnableBorderLayoutButtons(boolean value) {
    
    for(Component c : pnlBorderLayout.getComponents())
      c.setEnabled(value);
  }

  private int getMiddleHeight() {
    
    return 
      (Toolkit.getDefaultToolkit().getScreenSize().height / 2) - 
      (this.getHeight() / 2);
  }

  public JFrame getFrameToManipulate() {
    
    return frameToManipulate;
  }

  public void setFrameToManipulate(JFrame frameToManipulate) {
    
    this.frameToManipulate = frameToManipulate;
  }
}
