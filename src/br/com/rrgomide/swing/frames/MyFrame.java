package br.com.rrgomide.swing.frames;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
  
  /*
   * Paineis a serem incluídos
   * no frame
   */
  private JPanel panel1;
  private JPanel panel2;
  private JPanel panel3;
  
  /*
   * Botões a serem incluídos
   * nos paineis
   */
  private JButton botao1;
  private JButton botao2;
  private JButton botao3;
  
  /*
   * Botões a serem incluídos 
   * nos frames, após os
   * paineis
   */
  private JButton botao4;
  private JButton botao5;
  private JButton botao6;
  
  public MyFrame() {
    
    /*
     * Lembre-se que 'this' refere-se ao próprio objeto.
     * Nas 3 linhas abaixo, fazemos o seguinte:
     * (1) Definição do layout do frame (FlowLayout)
     * (2) Definição do tamanho do frame (640 x 480 pixels)
     * (3) Definição da operação padrão quando o frame é fechado (sair do programa, no caso)
     */
    this.setLayout(new FlowLayout());
    this.setSize(640, 480);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    /*
     * Criação de 3 paineis com cores
     * de fundo distintas
     */
    panel1  = new JPanel();
    panel1.setBackground(Color.BLACK);
    
    panel2  = new JPanel();
    panel2.setBackground(Color.BLUE);
    
    panel3 = new JPanel();
    panel3.setBackground(Color.RED);
    
    /*
     * Criação de 6 botões
     */
    botao1 = new JButton("Botão 1");
    botao2 = new JButton("Botão 2");
    botao3 = new JButton("Botão 3"); 
    botao4 = new JButton("Botão 4");
    botao5 = new JButton("Botão 5");
    botao6 = new JButton("Botão 6"); 
    
    /*
     * Adicionando 3 botões aos paineis,
     * através do método add (já existente
     * na classe JPanel)
     */
    panel1.add(botao1);
    panel2.add(botao2);
    panel3.add(botao3);
    
    /*
     * Adicionando todos os paineis (que contém
     * botões) e os demais botões ao frame, através
     * do método add (já existente na classe JFrame)
     */
    this.add(panel1);
    this.add(panel2);
    this.add(panel3);
    this.add(botao4);
    this.add(botao5);
    this.add(botao6);
  }
}

