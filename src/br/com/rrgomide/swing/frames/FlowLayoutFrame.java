package br.com.rrgomide.swing.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * Classe para demonstrar como o 
 * FlowLayout funciona
 * 
 * Criei com JDialog (e não JFrame)
 * para que eu consiga implementar o
 * comportamento "modal" (que não permite
 * a utilização de outras telas do programa
 * enquanto esta não for fechada)
 * 
 * @author Raphael
 *
 */
public class FlowLayoutFrame extends JDialog {

  /*
   * Botões a serem clicados pelo 
   * usuário
   */
  private JButton btnCreateButton;
  private JButton btnLeft;
  private JButton btnCenter;
  private JButton btnRight; 
  
  /*
   * Painéis para dividir as
   * funcionalidades
   * 
   * O painel do topo irá conter os 4
   * botões de interação
   * 
   * O painel do centro irá conter os 
   * botões que serão criados dinamicamente
   * para demonstrar como o FlowLayout funciona
   */
  private JPanel pnlTopo;
  private JPanel pnlCentro;
  
  /*
   * Contador para mostrar a quantidade de botões
   * criados 
   */
  private int counter = 1;
  
  /*
   * Objeto de layout, cujo alinhamento será modificado
   * durante a execução do programa
   */
  private FlowLayout mainLayout = new FlowLayout();
  
  /**
   * Construtor da classe
   */
  public FlowLayoutFrame() {
    
    frameConfiguration();
    configTopPanel();
    configCenterPanel();
  }

  private void frameConfiguration() {
       
    this.setTitle("Testes com FlowLayout");
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    this.setSize(640, 480);
    this.setLocationRelativeTo(null);
    this.setModal(true);
    this.setLayout(new BorderLayout(5, 5));
  }

  private void configTopPanel() {
    
    pnlTopo = new JPanel();
    
    /*
     * O layout deste painel (topo) é
     * o FlowLayout, mas este não
     * será controlado nesta aplicação
     */
    pnlTopo.setLayout(new FlowLayout());
    
    /*
     * Definindo borda vermelha para este painel
     */
    pnlTopo.setBorder(BorderFactory.createLineBorder(Color.RED, 5));
    
    /*
     * Aqui, criei um objeto da classe ButtonCreator, que implementa
     * a interface desejada para eventos em botões (ActionListener).
     * A classe ButtonCreator foi criada de forma interna (inner class)
     * e está no final deste arquivo.
     * 
     * Esta é uma das formas de se associar eventos a componentes (criar
     * a classe correspondente separadamente, criar um objeto e associar o
     * evento ao objeto. É útil principalmente quando desejamos reaproveitar
     * o código da classe interna em outros componentes
     */
    ButtonCreator buttonCreator = new ButtonCreator();
    btnCreateButton = new JButton("Crie novos botões");
    btnCreateButton.addActionListener(buttonCreator);    
    
    /*
     * Para o botão de alinhamento à esquerda, fiz a associação do evento
     * de forma mais simples, criando uma classe anônima, ou seja, sem nome.
     * 
     * Esta funcionalidade é muito utilizada em Java e deve ser explorada sempre
     * que possível. 
     * 
     * Classes anônimas são utilizadas principalmente para classes
     * pequenas tem utilidade bem específica, ou seja, não serão necessárias em
     * nenhuma outra parte do programa.
     */
    btnLeft = new JButton("Alinhar à esquerda");
    btnLeft.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        
        mainLayout.setAlignment(FlowLayout.LEFT);
        
        /*
         * Refletindo a alteração na tela, lembrando que
         * este layout está associando ao painel central
         * neste caso
         */
        mainLayout.layoutContainer(pnlCentro);
      }
    });
    
    /**
     * Para os botões de alinhamento ao centro e à direita, utilizei
     * a abordagem mais funcional e mais utilizada atualmente - lambdas,
     * que está disponível a partir do Java 8. Aqui, é possível utilizar
     * lambda porque a interface ActionListener implementa apenas 1 método:
     * actionPerformed. Nesses casos, ActionListener é considerada uma 
     * "Interface funcional" e pode ser substituída por um lambda.
     */
    btnCenter = new JButton("Alinhar ao centro");
    btnCenter
      .addActionListener(actionEvent -> {
        
        mainLayout.setAlignment(FlowLayout.CENTER);
        
        /*
         * Refletindo a alteração na tela, lembrando que
         * este layout está associando ao painel central
         * neste caso
         */
        mainLayout.layoutContainer(pnlCentro);
      });

    btnRight = new JButton("Alinhar à direita");
    btnRight
      .addActionListener(actionEvent -> {
        mainLayout.setAlignment(FlowLayout.RIGHT);
      
        /*
         * Refletindo a alteração na tela, lembrando que
         * este layout está associando ao painel central
         * neste caso
         */
        mainLayout.layoutContainer(pnlCentro);
      });
    
    /*
     * Adicionando os 4 botões recém-criados
     * ao painel do topo da tela
     */
    pnlTopo.add(btnCreateButton);
    pnlTopo.add(btnLeft);
    pnlTopo.add(btnCenter);
    pnlTopo.add(btnRight);
    
    /*
     * Por fim, adicionando o painel à tela, definindo
     * o posicionamento no topo (PAGE_START)
     */
    this.add(pnlTopo, BorderLayout.PAGE_START);
  }

  /*
   * Configurando o painel central
   */
  private void configCenterPanel() {
    
    pnlCentro = new JPanel();
    
    /*
     * Vinculando o layout do painel central com
     * o FlowLayout definido no construtor, ou seja,
     * que será controlado dinamicamente
     */
    pnlCentro.setLayout(mainLayout);
    
    /*
     * Definindo borda azul para o painel
     */
    pnlCentro.setBorder(BorderFactory.createLineBorder(Color.BLUE, 10));
    
    /*
     * Vinculando painel ao centro (BorderLayout [será visto em breve com detalhes])
     */
    this.add(pnlCentro, BorderLayout.CENTER);
  }
  
  /**
   * Classe interna (inner class) para a criação 
   * de novos botões dinamicamente
   * 
   * @author Raphael
   *
   */
  private class ButtonCreator implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {      
      
      pnlCentro.add(new JButton("Fui criado dinamicamente (" + counter + ")"));
      counter++;
      
      /*
       * Repintando a tela para refletir
       * as modificações
       */
      mainLayout.layoutContainer(pnlCentro);
    }    
  }
}
