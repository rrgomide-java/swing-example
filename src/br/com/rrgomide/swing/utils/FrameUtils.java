package br.com.rrgomide.swing.utils;

import java.awt.Component;

import javax.swing.JFrame;

/**
 * Pensei em criar uma classe com métodos
 * auxiliares para frames, mas não evolui 
 * muito, por enquanto
 * @author m24107
 *
 */
public class FrameUtils {

  public static void i(JFrame frame, Component component) {
    frame.add(component);
  }
}
