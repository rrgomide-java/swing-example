package br.com.rrgomide.swing.test;

import javax.swing.JFrame;
import br.com.rrgomide.swing.frames.MyFrame;

public class JFramePanelButtonTest {

  public static void main(String[] args) {

    /*
     * Instanciando classe MyFrame
     */
    JFrame myFrame = new MyFrame();
    
    /*
     * Exibindo o frame na tela
     */
    myFrame.setVisible(true);
  } 
}
