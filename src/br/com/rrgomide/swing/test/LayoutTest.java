package br.com.rrgomide.swing.test;

import javax.swing.JFrame;

import br.com.rrgomide.swing.frames.LayoutControlFrame;

public class LayoutTest {

  public static void main(String[] args) {

    JFrame frameToManipulate = new JFrame();
    LayoutControlFrame layoutFrame = new LayoutControlFrame(frameToManipulate);
    layoutFrame.setVisible(true);
  }
}
