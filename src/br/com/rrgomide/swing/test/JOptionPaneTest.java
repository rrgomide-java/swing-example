package br.com.rrgomide.swing.test;

import javax.swing.JOptionPane;

public class JOptionPaneTest {

  public static void main(String[] args) {

    /*
     * Chamada de métodos em sequência
     */
    testeJOptionPane();
    testeJOptionPanelMensagens();
    testeJOptionPaneInputFull();
  }  

  public static void testeJOptionPane() {
    
    /*
     * Obtendo dois números do usuário no formato String, por enquanto
     * (é o que showInputDialog retorna por padrão)
     */
    String num1 = JOptionPane.showInputDialog("Informe o primeiro número");  
    String num2 = JOptionPane.showInputDialog("Informe o segundo  número"); 
    
    /*
     * Obtendo os números através da conversão de String para int
     */
    int op1 = Integer.parseInt(num1);
    int op2 = Integer.parseInt(num2);
    
    /*
     * Mostrando mensagem ao usuário contendo a soma dos dois números, 
     * utilizando uma das combinações de parâmetros para showMessageDialog
     */
    JOptionPane.showMessageDialog(
      null, 
      String.format("A soma dos números é: %d", (op1 + op2)), 
      "Soma", 
      JOptionPane.PLAIN_MESSAGE
    );  
  }

  private static void testeJOptionPanelMensagens() {
    
    /*
     * Outra combinação de parâmetros para
     * showMessageDialog
     */
    JOptionPane.showMessageDialog(
      null,
      "Mensagem",
      "Título",
      JOptionPane.ERROR_MESSAGE      
    );
  }

  private static void testeJOptionPaneInputFull() {
    
    /*
     * Criando um array de Object para o showInputDialog
     */
    Object[] opcoes = new Object[2];
    opcoes[0] = "Opção X";
    opcoes[1] = "Opção Y";
    
    /*
     * Criando um showInputDialog mais complexo (com mais
     * opções de parâmetros)
     */
    Object retorno = JOptionPane.showInputDialog(
      null, 
      "Mensagem", 
      "Título", 
      JOptionPane.WARNING_MESSAGE, 
      null, 
      opcoes,
      "Opção Y"
      );
    
    /*
     * Exibindo a opção escolhida pelo usuário
     */
    JOptionPane.showMessageDialog(null, retorno);
  }
}
