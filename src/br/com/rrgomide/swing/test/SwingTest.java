package br.com.rrgomide.swing.test;

import javax.swing.JFrame;

import br.com.rrgomide.swing.frames.MainFrame;

public class SwingTest {

  public static void main(String[] args) {

    MainFrame mainFrame = new MainFrame();
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainFrame.setVisible(true);
  }
}
